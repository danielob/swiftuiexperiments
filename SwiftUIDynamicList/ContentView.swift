import SwiftUI
import Combine

struct ItemModel {
    var value: Int
    var isOn: Bool
}

final class ItemViewModel: ObservableObject, Identifiable {
    @Published private(set) var model: ItemModel
    
    init(value: Int) {
        model = ItemModel(value: value, isOn: false)
    }
    
    func title() -> String {
        return String(model.value)
    }
    
    func updateToggle(_ isOn: Bool) {
        model.isOn = isOn
    }
}

struct ItemRowView: View {
    @ObservedObject private var viewModel: ItemViewModel
    
    init(_ viewModel: ItemViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        Toggle(isOn: bindToggle()) {
            HStack {    Text(viewModel.title()) }
        }
    }
    
    private func bindToggle() -> Binding<Bool> {
        Binding(
            get: {
                self.viewModel.model.isOn
            },
            set: { value in
                self.viewModel.updateToggle(value)
            }
        )
    }
}

final class ListViewModel: ObservableObject, Identifiable {
    @Published private var sum: Int = 0
    
    var footerText: String {
        "Sum is \(sum)"
    }
    var canDisplaySecret: Bool {
        sum == 8
    }
    
    private(set) var items = [ItemViewModel]()
    
    private var bag = Set<AnyCancellable>()
    
    init() {
        configure()
    }

    private func configure() {
        for i in 1...4 {
            let childViewModel = ItemViewModel(value: i)
            observeChildViewModelUpdate(childViewModel)
            items.append(childViewModel)
        }
    }
    
    private func observeChildViewModelUpdate(_ childViewModel: ItemViewModel) {
        childViewModel
            .$model
            .dropFirst()
            .sink(receiveValue: updateSum)
            .store(in: &bag)
    }
    
    func reset() {
        items.forEach { $0.updateToggle(false) }
    }
    
    private func updateSum(_ model: ItemModel) {
        guard isItemModelDiffentThanPreviousState(model) else { return }
        sum += model.isOn ? model.value : -model.value
    }
    
    private func isItemModelDiffentThanPreviousState(_ model: ItemModel) -> Bool {
        items[model.value - 1].model.isOn != model.isOn
    }
}

struct SecretSection: View {
    var body: some View {
        Section(header: Text("Secret")) {
            Text("You found the secret 🤫")
        }
    }
}

struct ContentView: View {
    @ObservedObject private var viewModel = ListViewModel()

    var body: some View {
        NavigationView {
            List {
                Section(header: Text("Numbers"), footer: Text(viewModel.footerText)) {
                    ForEach(viewModel.items, content: ItemRowView.init)
                }
                
                if viewModel.canDisplaySecret {
                    SecretSection()
                }
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitle("My List")
            .navigationBarItems(trailing:
                Button("Reset") {
                    self.viewModel.reset()
                }
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
